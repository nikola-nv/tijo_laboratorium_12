package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank;
import spock.lang.Specification
import spock.lang.Unroll;

class TransferSpec extends Specification {
    @Unroll
    def "performs a transfer from account number #fromAccount to #toAccount with amount #amount"() {

        given: "initial data"
        def bank = new Bank()
        when: "performs a transfer"
        def check = bank.transfer(fromAccount , toAccount , amount)
        then: "check if operation is successful"
        result == check

        where:
        fromAccount | toAccount || amount || result
        2           | 3         || 100    || true
        3           | 4         || 101    || true
        2           | 4         || 102    || false
    }
}
