package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank;
import spock.lang.Specification
import spock.lang.Unroll;

class DepositSpec extends Specification {
    @Unroll
    def "executes the payment for account number #accountNumber with amount #amount"() {

        given: "initial data"
        def bank = new Bank()
        when: "executes the payment"
        def exist = bank.deposit(accountNumber , amount)
        then: "check if operation is successful"
        exist == expected

        where:
        amount | accountNumber || expected
        100    | 1             || true
        101    | 2             || true
        102    | 3             || true
        103    | 4             || true
    }
}
