package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank;
import spock.lang.Specification
import spock.lang.Unroll;

class SumAccountsBalanceSpec extends Specification {
    @Unroll
    def "summaries account balance of #accountCount users"() {

        given: "initial data"
        def bank = new Bank()
        when: "summaries account balances"
        def number = bank.sumAccountsBalance()
        then: "check amount of summaries"
        number == summaries

        where:
        summaries | accountCount
        306       | 3
    }
}
