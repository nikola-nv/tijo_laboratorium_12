package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank
import spock.lang.Specification
import spock.lang.Unroll
class AccountBalanceSpec extends Specification {
    @Unroll
    def "return account balance for account number #accountNumber with balance #balance"() {

        given: "initial data"
        def bank = new Bank()
        when: "take account balance"
        def accountBalance = bank.accountBalance(accountNumber)
        then: "check expected balance"
        accountBalance == balance

        where:
        balance | accountNumber
            100 | 1
            101 | 2
            102 | 3
            103 | 4
    }
}
