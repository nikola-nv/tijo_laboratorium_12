package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank;
import spock.lang.Specification
import spock.lang.Unroll;

class DeleteAccountSpec extends Specification {
    @Unroll
    def "should delete account number #accountNumber with balance #balance"() {

        given: "initial data"
        def bank = new Bank()
        when: "the account is deleted"
        def accountBalance = bank.deleteAccount(accountNumber)
        then: "check account balance"
        accountBalance == balance

        where:
        balance   | accountNumber
        100       |  1
    }
}
