package pl.edu.pwsztar

import pl.edu.pwsztar.service.impl.Bank;
import spock.lang.Specification
import spock.lang.Unroll;

class WithdrawSpec extends Specification {
    @Unroll
    def "executes funds withdrawal from the account number #accountNumber with amount #amount"() {

        given: "initial data"
        def bank = new Bank()
        when: "executes funds withdrawal"
        def result = bank.withdraw(accountNumber,amount)
        then: "check if operation is successful"
        result == expected

        where:
        accountNumber | amount        || expected
        1             | 100           || false
        2             | 101           || false
        3             | 102           || false
        4             | 200           || true
    }
}
