package pl.edu.pwsztar.service.impl;

import pl.edu.pwsztar.data.CounterManager;
import pl.edu.pwsztar.data.DataManager;
import pl.edu.pwsztar.data.impl.CounterManagerImpl;
import pl.edu.pwsztar.data.impl.DataManagerImpl;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.mapper.CreateAccountMapper;
import pl.edu.pwsztar.domain.model.Account;
import pl.edu.pwsztar.service.BankOperation;

import java.util.ArrayList;
import java.util.List;

public class Bank implements BankOperation {
    private final DataManager data;
    private final CounterManager count;

    private final Converter<Integer , Account> createAccountMapper;

    public Bank (){
        this.data = new DataManagerImpl();
        this.count = new CounterManagerImpl();
        this.createAccountMapper = new CreateAccountMapper();
    }

    public int createAccount() {
        int accountNumber = count.getCount();
        List<Account> accounts = data.getClients();
        Account createdAccount = null;

        ++accountNumber;
        createdAccount = createAccountMapper.convert(accountNumber);
        accounts.add(createdAccount);

        count.saveCount(accountNumber);
        data.saveClients(accounts);

        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        List<Account> accounts = data.getClients();
        List<Account> updatedAccounts = new ArrayList<>();
        int result = -1;

        for(Account account: accounts){
            if(account.getAccountNumber() == accountNumber)
            {
                result = account.getAccountBalance();
            }
            else
            {
                updatedAccounts.add(account);
            }
        }

        if(result != -1) {
            data.saveClients(updatedAccounts);
            return result;
        }

        return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        List<Account> accounts = data.getClients();
        List<Account> updatedAccounts = new ArrayList<>();
        boolean result = false;

        for(Account account: accounts){
            if(account.getAccountNumber() == accountNumber)
            {
                account = new Account.Builder().accountNumber(account.getAccountNumber()).accountBalance(account.getAccountBalance()+amount).build();
                result = true;
            }
            updatedAccounts.add(account);
        }

        if(result) {
            data.saveClients(updatedAccounts);
        }

        return result;
    }

    public boolean withdraw(int accountNumber, int amount) {
        List<Account> accounts = data.getClients();
        List<Account> updatedAccounts = new ArrayList<>();
        boolean result = false;

        for(Account account: accounts){
            if(account.getAccountNumber() == accountNumber && account.getAccountBalance()-amount >= 0)
            {
                    account = new Account.Builder().accountNumber(account.getAccountNumber()).accountBalance(account.getAccountBalance() - amount).build();
                    result = true;
            }
            updatedAccounts.add(account);
        }

        if(result) {
            data.saveClients(updatedAccounts);
        }

        return result;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        List<Account> accounts = data.getClients();
        List<Account> firstUpdatedAccounts = new ArrayList<>();
        List<Account> secondUpdatedAccounts = new ArrayList<>();
        boolean firstResult = false;
        boolean secondResult = false;

        for(Account account: accounts){
            if(account.getAccountNumber() == fromAccount && account.getAccountBalance()-amount >= 0)
            {
                account = new Account.Builder().accountNumber(account.getAccountNumber()).accountBalance(account.getAccountBalance() - amount).build();
                firstResult = true;
            }
            firstUpdatedAccounts.add(account);
        }

        for(Account account: firstUpdatedAccounts){
            if(account.getAccountNumber() == toAccount)
            {
                account = new Account.Builder().accountNumber(account.getAccountNumber()).accountBalance(account.getAccountBalance() + amount).build();
                secondResult = true;
            }
            secondUpdatedAccounts.add(account);
        }

        if(firstResult && secondResult) {
            data.saveClients(secondUpdatedAccounts);
        }

        return firstResult && secondResult;
    }

    public int accountBalance(int accountNumber) {
        List<Account> accounts = data.getClients();

        for(Account account: accounts){
            if(account.getAccountNumber() == accountNumber)
            {
                return account.getAccountBalance();
            }
        }

        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        List<Account> accounts = data.getClients();
        int summaries = 0;

        for(Account account: accounts){
           summaries += account.getAccountBalance();
        }

        return summaries;
    }
}
