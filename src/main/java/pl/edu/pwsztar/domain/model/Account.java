package pl.edu.pwsztar.domain.model;

import java.io.Serializable;

public class Account implements Serializable {
    private int accountNumber;
    private int accountBalance;

    private Account(Builder builder){
        this.accountNumber = builder.accountNumber;
        this.accountBalance = builder.accountBalance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public static final class Builder{
        private int accountNumber;
        private int accountBalance;

        public Builder accountNumber(int accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder accountBalance(int accountBalance) {
            this.accountBalance = accountBalance;
            return this;
        }

        public Account build(){
            return new Account(this);
        }
    }
}
