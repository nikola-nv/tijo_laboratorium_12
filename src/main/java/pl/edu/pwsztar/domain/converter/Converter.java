package pl.edu.pwsztar.domain.converter;

public interface Converter<F,T> {
    T convert(F from);
}
