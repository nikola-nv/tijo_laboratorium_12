package pl.edu.pwsztar.domain.mapper;

import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.model.Account;

public class CreateAccountMapper implements Converter<Integer , Account> {
    @Override
    public Account convert(Integer accountNumber) {
        return new Account.Builder().accountNumber(accountNumber).accountBalance(0).build();
    }
}
