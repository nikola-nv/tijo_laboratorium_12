package pl.edu.pwsztar.data.impl;

import pl.edu.pwsztar.data.CounterManager;
import pl.edu.pwsztar.data.FileMain;

import java.io.*;

public class CounterManagerImpl implements CounterManager {
    private final FileMain fileMain;

    public CounterManagerImpl(){
        this.fileMain = new FileMainImpl();
    }

    @Override
    public void saveCount(int accountNumber) {
        File file = fileMain.createCounterFile();
        BufferedWriter buff = null;

        try{
            buff = new BufferedWriter(new FileWriter(file));

            buff.write(accountNumber);

        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally {

            try {
                if(buff!=null) {
                    buff.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getCount() {
        File file = fileMain.createCounterFile();
        BufferedReader buff = null;
        int accountCount = 0;

        try{
            buff = new BufferedReader(new FileReader(file));

            accountCount = buff.read();

            return accountCount;
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally {

            try {
                if(buff!=null) {
                    buff.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}
