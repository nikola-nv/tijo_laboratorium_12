package pl.edu.pwsztar.data.impl;

import pl.edu.pwsztar.data.DataManager;
import pl.edu.pwsztar.data.FileMain;
import pl.edu.pwsztar.domain.model.Account;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataManagerImpl implements DataManager {
    private final FileMain fileMain;

    public DataManagerImpl(){
        this.fileMain = new FileMainImpl();
    }

    @Override
    public void saveClients(List<Account> accounts) {
        ObjectOutputStream oos = null;
        File file = fileMain.createDataFile();

        try{
            oos = new ObjectOutputStream(new FileOutputStream(file));

            for(Account account : accounts){
                oos.writeObject(account);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            try {
                if(oos!=null) {
                    oos.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Account> getClients() {
        ObjectInputStream ois = null;
        File file = fileMain.createDataFile();
        List<Account> accounts = new ArrayList<>();

        try{
            ois = new ObjectInputStream(new FileInputStream(file));

            while(true){
                try {
                    accounts.add((Account)ois.readObject());
                }catch (EOFException e){
                    return accounts;
                }
            }
        }
        catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        finally {

            try {
                if(ois!=null) {
                    ois.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }

        return null;
    }
}
