package pl.edu.pwsztar.data.impl;

import pl.edu.pwsztar.data.FileMain;
import pl.edu.pwsztar.domain.model.Account;

import java.io.*;

public class FileMainImpl implements FileMain {

    @Override
    public File createDataFile() {
        File file = null;
        ObjectOutputStream ois = null;

        try {
            file = new File("client_data.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            if (file.isFile() && file.length() == 0) {
                ois = new ObjectOutputStream(new FileOutputStream(file));
                ois.writeObject(new Account.Builder().accountNumber(0).accountBalance(0).build());
            }
            return file;
        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(ois!=null) {
                    ois.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public File createCounterFile() {
        File file = null;
        BufferedWriter buff = null;

        try {
            file = new File("account_counter.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            if (file.isFile() && file.length() == 0) {
                buff = new BufferedWriter(new FileWriter(file));
                buff.write(0);
            }
            return file;
        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(buff!=null) {
                    buff.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
