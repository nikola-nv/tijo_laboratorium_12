package pl.edu.pwsztar.data;

public interface CounterManager {
    void saveCount(int accountNumber);
    int getCount();
}
