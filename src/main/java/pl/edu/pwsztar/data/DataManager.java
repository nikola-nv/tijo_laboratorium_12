package pl.edu.pwsztar.data;

import pl.edu.pwsztar.domain.model.Account;

import java.util.List;

public interface DataManager {
    void saveClients(List<Account> accounts);
    List<Account> getClients();
}
