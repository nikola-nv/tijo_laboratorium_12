package pl.edu.pwsztar.data;

import java.io.File;

public interface FileMain {
    File createDataFile();
    File createCounterFile();
}
